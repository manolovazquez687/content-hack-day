### Instructions: 

So you want to write about something for Marketing Hack Day? Great! Please share
your idea using the template below so we know what to expect. 

**Title**:

**Summary**:

**Keywords, themes**:

Now create a blog post MR ([see instructions](https://about.gitlab.com/handbook/marketing/blog/#styles-guidelines)).
Don’t forget to fill in all the relevant frontmatter. When you’re happy with it,
assign to @rebecca to review. Feel free to ping the content team (@rebecca, 
@erica, @evhoffmann, @atflowers, @suripatel) if you have questions or get stuck. 