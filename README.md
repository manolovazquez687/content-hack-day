# Content Hack Day

Please see [the handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/content-hack-day/) for details.

Join the Content team for a day of content creation! GitLab is home to a host 
of experts in all subjects, and we know you have a ton of knowledge, expertise, 
and insight to share. We need your help to create content that only you could write.

Join #content-hack-day on Slack to stay up to date and ask questions.

## How it works

We're dedicating a day to writing content for GitLab. We're hoping to 
come away with a ton of team blog posts to publish on the blog over the next few months!

While the content team will be on it the whole day, team members are free to join in for 
however long they are free. We'll have an open Zoom meeting on the day for people
to join to discuss ideas and get editorial help. 

Check out the issues for ideas and inspiration – we’ve created some topics that 
we need covered to get you started, but don’t stop there! If you have an experience
to write about, an experiment to share, a challenge you overcame – we want to 
hear about it. Assign yourself to an existing issue or create a new one using 
the `team-ideas` issue template. Read through [the blog handbook](https://about.gitlab.com/handbook/marketing/blog/) before you get 
started and feel free to rope in members of the content team for help with structuring your story. You don't need to have a fully formed idea to get going!

### Some prompts to inspire you:

- What do I wish I had known before I attempted to do something?  (i.e. write the post you wish someone else had written before you!)
- What stories have you read recently that you found interesting/helpful/surprising?
- Do you use GitLab for something other than its intended purpose? Might someone else find that useful?

Some good questions to ask yourself before you start:

- What do I want others to learn from this story? 
- What makes me the best person to write about this?

#### Random giveaways

We'll also be giving away some swag randomly throughout the day!